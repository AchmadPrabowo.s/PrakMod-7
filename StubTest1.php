<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	include "MyClass.php";
	class StubTest1 extends TestCase{
		public function testStub(){
			$stub = $this->createMock(MyClass::class);
			$stub->method('MyMethod')->willReturn("foo");
			$this->assertSame("foo", $stub->MyMethod());
		}
	}
?>